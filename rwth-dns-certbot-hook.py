#!/usr/bin/python3
# Certbot-Hook for RWTH-DNS created by Johannes Sprink
# Date: 01.03.2022
# GIT-Repo: https://git.rwth-aachen.de/johannes.sprink/rwth-dns-certbot-hook

import logging
import os
import re
import sys
import time

import requests

# logging.basicConfig(level=logging.DEBUG)
# requests_log = logging.getLogger("requests.packages.urllib3")
# requests_log.setLevel(logging.DEBUG)
# requests_log.propagate = True

API_TOKEN = "<%= @rwth_dns_api_token %>"

LIST_ZONES_URL = "https://noc-portal.rz.rwth-aachen.de/dns-admin/api/v1/list_zones"
LIST_RECORDS_URL = "https://noc-portal.rz.rwth-aachen.de/dns-admin/api/v1/list_records"
CREATE_RECORD_URL = "https://noc-portal.rz.rwth-aachen.de/dns-admin/api/v1/create_record"
UPDATE_RECORD_URL = "https://noc-portal.rz.rwth-aachen.de/dns-admin/api/v1/update_record"
DELETE_RECORD_URL = "https://noc-portal.rz.rwth-aachen.de/dns-admin/api/v1/destroy_record"
DEPLOY_ZONE_URL = "https://noc-portal.rz.rwth-aachen.de/dns-admin/api/v1/deploy_zone"

ZONE_PATTERN = re.compile(r"^(.*\.)?([\w-]+\.rwth-aachen\.de)$")  # regex-pattern to find zone from hostname


def _assert_request_status_code(r, message=None, status_code=200):
    if r.status_code != status_code:
        if (
            r.status_code == 422
            and r.json().get("hostname_name") == ["has already been taken"]
            and r.json().get("text") == ["has already been taken"]
        ):
            logging.warning("Requested record already existed, not creating new one.")
        else:
            raise Exception(
                "{}: Response-Status-Code {}, Response-body: {}".format(
                    message or "Request could not be fulfilled", r.status_code, r.text
                )
            )


def get_zone_name(domain):
    """Get the base of a given URL.

    eg. 'host.test.rwth-aachen.de' results in 'test.rwth-aachen.de'"""

    if not domain.endswith("rwth-aachen.de"):
        raise Exception('This script only works for ".rwth-aachen.de"-Domains')

    x = ZONE_PATTERN.match(domain)
    return x.group(2)


def get_zone_id(zone_name):
    r = requests.get(LIST_ZONES_URL, headers={"PRIVATE-TOKEN": API_TOKEN}, data={"search": "*"})  # TODO: use search?
    _assert_request_status_code(r, "Zone-ID could not be requested")
    for zone_info in r.json():
        if zone_info.get("zone_name") == zone_name:
            return zone_info["id"]
    raise Exception("Zone not found!")


def get_changable_acme_challenge_entrys(domain, zone_id):
    """If TXT-Records with "_acme-challenge.{domain}" exist and has not been deployed recently, their IDs."""
    r = requests.get(
        LIST_RECORDS_URL,
        headers={"PRIVATE-TOKEN": API_TOKEN},
        data={"zone-id": zone_id, "search": f"_acme-challenge.{domain}*"},
    )
    _assert_request_status_code(r, "Existing ACME-Challenges could not be read")
    return r.json()


def get_first_changable_acme_challenge_entry_id(domain, zone_id):
    """If a TXT-Record with "_acme-challenge.{domain}" exist and has not been deployed recently, get the first ID."""
    for entry in get_changable_acme_challenge_entrys(domain, zone_id):
        if entry.get("type") == "txt_record" and entry.get("status") == "deployed" and entry.get("editable") is True:
            return entry.get("id")


def create_new_txt_record(domain, content, comment=None):
    record_content = f'_acme-challenge.{domain} IN TXT "{content}"'
    if comment:
        record_content += f" ; {comment}"
    r = requests.post(
        CREATE_RECORD_URL,
        headers={"PRIVATE-TOKEN": API_TOKEN},
        data={"record_content": record_content},
    )
    _assert_request_status_code(r, f'New record "{record_content}" could not be created')
    return r.json().get("id")


def update_existing_txt_record(record_id, domain, content, comment=None):
    record_content = f'_acme-challenge.{domain} IN TXT "{content}"'
    if comment:
        record_content += f" ; {comment}"
    r = requests.post(
        UPDATE_RECORD_URL,
        headers={"PRIVATE-TOKEN": API_TOKEN},
        data={"record_id": record_id, "record_content": record_content},
    )
    _assert_request_status_code(r, f'Record {record_id} could not be changed to "{record_content}"')
    return r.json().get("id")


def create_txt_acme_challenge_entry(domain, zone_id, challenge_text):
    if existing_entry_id := get_first_changable_acme_challenge_entry_id(domain, zone_id):
        update_existing_txt_record(existing_entry_id, domain, challenge_text)
    else:
        create_new_txt_record(domain, challenge_text)


def delete_record(record_id):
    r = requests.delete(
        DELETE_RECORD_URL,
        headers={"PRIVATE-TOKEN": API_TOKEN},
        data={"record_id": record_id},
    )
    _assert_request_status_code(r, f"Record {record_id} could not be deleted")


def cleanup_existing_acme_challenge_entries(domain, zone_id):
    """Delete all acme-challenges for the requested domain from the 'Existing Records'."""
    for entry in get_changable_acme_challenge_entrys(domain, zone_id):
        if entry.get("type") == "txt_record" and entry.get("status") == "deployed" and entry.get("editable") is True:
            delete_record(entry.get("id"))


def deploy_zone(zone_id):
    r = requests.post(
        DEPLOY_ZONE_URL,
        headers={"PRIVATE-TOKEN": API_TOKEN},
        data={"zone_id": zone_id},
    )
    _assert_request_status_code(r, f"Zone {zone_id} could not be deployed")


def deploy_changes(wait_for_deployment):
    """If this is the last challenge for the certbot-run, tell the DNS-Server to deploy all changes."""
    if certbot_remaining_challenges == "0":
        all_zone_names = set(get_zone_name(domain.strip()) for domain in certbot_all_domains.split(","))
        for zone in all_zone_names:
            deploy_zone(get_zone_id(zone))

        if wait_for_deployment:
            # Wait until RWTH-DNS is deployed again (all 15 minutes)
            seconds_since_last_deployment = time.time()

            # assume clock of client is 20s off for safety
            # always wait two minutes longer to give the server time to deploy the DNS-zone
            seconds_to_wait = (17 * 60 + 20) - (seconds_since_last_deployment + 20) % (15 * 60)

            logging.debug(f"Waiting for {int(seconds_to_wait)}s before deployment")
            time.sleep(seconds_to_wait)

# Certbot gives the required parameters as environment-variables:
for i in [
    "CERTBOT_DOMAIN",
    "CERTBOT_VALIDATION",
    "CERTBOT_TOKEN",
    "CERTBOT_REMAINING_CHALLENGES",
    "CERTBOT_ALL_DOMAINS",
]:
    logging.debug(" '{}': {}".format(i, os.environ.get(i)))

# Run the main loop when ran interactively
if __name__ == "__main__":
    certbot_domain = os.environ.get("CERTBOT_DOMAIN")
    certbot_challenge_text = os.environ.get("CERTBOT_VALIDATION")
    certbot_remaining_challenges = os.environ.get("CERTBOT_REMAINING_CHALLENGES")
    certbot_all_domains = os.environ.get("CERTBOT_ALL_DOMAINS")

    zone_id = get_zone_id(get_zone_name(certbot_domain))

    if "CERTBOT_AUTH_OUTPUT" in os.environ:
        # This call is the cleanup-call!
        certbot_auth_output = os.environ.get("CERTBOT_AUTH_OUTPUT")

        cleanup_existing_acme_challenge_entries(certbot_domain, zone_id)
        deploy_changes(wait_for_deployment=False)
    else:
        # create the necessary TXT-records:
        create_txt_acme_challenge_entry(certbot_domain, zone_id, certbot_challenge_text)
        deploy_changes(wait_for_deployment=True)
