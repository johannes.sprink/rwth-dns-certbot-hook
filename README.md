# Certbot hook for RWTH-DNS-API

DNS Authenticator plugin für [Certbot](https://certbot.eff.org/).

This plugin uses the DNS-API provided by RWTH Aachen University to proove the ownership of a `.rwth-aachen.de`-domain to an ACME-Cert-Provider. 

## Usage
1. Create a token at [DNS-Admin](https://noc-portal.rz.rwth-aachen.de/dns-admin/de/api_tokens) (access restricted to RWTH-IPs)
2. Replace  `<%= @rwth_dns_api_token %>` with your aquired token (the displayed BASE64-string)
3. Run certbot with parameters `--manual-auth-hook` and `--manual-cleanup-hook`

## Example

To aquire a wildcard-cert for `*.wildhost.inst.rwth-aachen.de` you could use this command:

```certbot --keep-until-expiring --text --agree-tos --non-interactive certonly --rsa-key-size 4096 --cert-name 'wildhost.inst.rwth-aachen.de' -d '*.wildhost.inst.rwth-aachen.de' -d 'wildhost.inst.rwth-aachen.de' --post-hook "/bin/systemctl restart nginx.service" --server https://acme-staging-v02.api.letsencrypt.org/directory --manual --preferred-challenges dns --manual-cleanup-hook /scriptpath/rwth-dns-certbot-hook.py --manual-auth-hook /scriptpath/rwth-dns-certbot-hook.py```

## Tips and Hints

Heads up: this script waits for the next deployment of the RWTH-DNS-Server, which happens about every 15 minutes. So it might take a while to run, but just be patient and let it do its thing.

### Permissions
Tokens can be restriced to specific actions. The token needs at least this permissions:
*  List zones
*  List records
*  Deploy zones
*  Create TXT Records
*  Update TXT Records
*  Destroy TXT Records

### Hostname restrictions
Each DNS-token can be created with **Hostname restrictions**. If you use those, make sure to include the base zone into the restictions (for `list zones` and `deploy zones`). In the example above, you need to include the hostnames into the list: `*.wildhost.inst.rwth-aachen.de`, and `inst.rwth-aachen.de`

## Help and Feedback

Thanks for checking out my script! I know it's not perfect, and I'm always looking for ways to make it better. So if you have any ideas or suggestions for improvements, feel free to let me know by submitting a pull request or an issue.

